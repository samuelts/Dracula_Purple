# Dracula_Purple
A Dracula Theme Setup for Plasma 5 w/ GTK App Support

### Notes
This has only been verified working on my personal machine, use at your own risk for now.

My system is running Manjaro Linux with 

  * KDE Plasma Version 5.14.3
  * KDE Frameworks Version 5.52.0
  * Qt Version: 5.11.2
  * Kernel Version: 4.19.4-1-MANJARO

# Images
![Screenshot](./assets/screenshot.png)

# Installation

## Prep

### Wallpaper
[Inside a Magical Cave](https://www.reddit.com/r/wallpapers/comments/a10crl/inside_a_magical_cave_3033x2000/)

### Font
DE - [Noto Sans](https://www.google.com/get/noto/)

Terminal - [Meslo LG M Regular for Powerline](https://github.com/powerline/fonts/tree/master/Meslo%20Slashed)

### Window Decoration
  1. Ensure you have selected the Breeze Style under _System Settings > Application Style > Window Decorations_
  2. From the Window Decorations menu click _Configure Breeze > Shadows_ and set

    Size: Small
    Strength: 100%
    Color: #000000

### Icons
  1. Install the Flat-Remix Iconset from [here](https://store.kde.org/p/1012430/) or via _System Settings > Icons > Get New Themes_
  2. Enable Flat-Remix from _System Settings > Icons_

### Third Party Applications
#### Required
  * [Latte Dock](https://github.com/psifidotos/Latte-Dock) - Enable advanced mode in dock settings
  * [Kvantum](https://github.com/tsujan/Kvantum) - (kvantum-qt5 via yaourt on Manjaro)

#### Optional
  * [Telegram Desktop](https://desktop.telegram.org/) - [Telegram Dracula Theme](https://draculatheme.com/telegram/)
  * [Sublime Text](https://www.sublimetext.com/) - [Package Control](https://packagecontrol.io/installation)
  * [Dracula Color Scheme](https://packagecontrol.io/packages/Dracula%20Color%20Scheme) - [DA UI](https://packagecontrol.io/packages/DA%20UI) (installed via command palette - Ctrl+Shift+P inside Sublime)
  * ZSH with [Zim](https://github.com/zimfw/zimfw) and [Pure](https://github.com/sindresorhus/pure)
  * neofetch is piped through lolcat to get the rainbow effect

## Latte Dock

### Bottom Dock
Setup is nearly default for the dock, the only changes I have made are to disable zoom on hover.

### Dock Widgets
  * Application Dashboard
  * Latte Plasmoid

### Panel
#### Behavior
    
    Position: Top
    Alignment: Justify
    Visibility: Always Visible

#### Appearance

    Size: 24px
    Zoom on Hover: 0%
    Background: Show, Size: 100%, Opacity: 50%

#### Tweaks
    
    Blur for panel background
    Hide panel shadow for maximized windows
    
### Panel Widgets 
  * Application Title (Left)
  * Global Menu (Left)
  * Justify Spacer
  * Digital Clock (Center)
  * Justify Spacer
  * Color Picker
  * Trash
  * System Tray

## GTK
  1. Copy Ant-Dracula-Purple folder to /usr/share/themes/ or ~/.themes/
  2. Set the theme in _System Settings > Application Style > GNOME Application Style (GTK)_ for both GTK2 and GTK3

## KDE Color Scheme
  1. Copy Dracula_Purple.colors to ~/.local/share/color-schemes/
  2. Set the color scheme in _System Settings > Colors_

## Konsole Color Scheme
  1. Copy Dracula_Purple.colorscheme to ~/.local/share/konsole/
  2. Set the color scheme in Konsole under _Edit Current Profile > Appearance_

## Kvantum
  1. Copy KvDracula folder to ~/.config/Kvantum/
  2. Select the theme from the Kvantum Theme Manager
  3. In Kvantum under _Configure Active Theme > Hacks_ set the following (feel free to tweak to your liking):
    ![Hacks](./assets/kvantum_hacks.png)
  4. Under _Compositing & General Look_ set the following
    ![Compositing & General Look](./assets/kvantum_composite.png)
  5. Leave _Sizes & Delays_ unchanged.
  6. Under _Miscellaneous_ set the following
    ![Miscellaneous](./assets/kvantum_misc.png)

# Credits

* [Zeno Rocha](https://zenorocha.com/) - [Dracula](https://draculatheme.com)
* [EliverLara](https://github.com/EliverLara) - [Ant-Dracula](https://github.com/EliverLara/Ant-Dracula)
* [tsujan](https://github.com/tsujan) - [Kvantum](https://github.com/tsujan/Kvantum)
* [Andrew Palyanov](https://www.artstation.com/redish) - [Wallpaper](https://www.artstation.com/artwork/NKK6d)